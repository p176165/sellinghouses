var db = require("../db");
var dateFormat = require('dateformat')

let model = {
    getSite_plans: (cb) => {
        return db.query("SELECT * FROM site_plans", cb)
    },
    getSite_plan: (id, cb) => {
        return db.query("SELECT * FROM site_plans WHERE id=?", [id], cb)
    },
    addSite_plan: (input, cb) => {

       console.log(input);
        return db.query("INSERT INTO site_plans SET ?", [input], cb)
    },
    updateSite_plan: (input, cb) => {
        
        return db.query("UPDATE site_plans SET ? WHERE id=?", [input, input.id], cb)
    },
    deleteSite_plan: (id, cb) => {
        return db.query("DELETE FROM site_plans WHERE id=?", [id], cb);
    },
    getSite_planF: (id, cb) => {
        return db.query("SELECT * FROM site_plans WHERE development=?", [id], cb)
    }
}

module.exports = model;