var db = require("../db");
var dateFormat = require('dateformat')

let model = {
    getHouseTypes: (cb) => {
        return db.query("SELECT * FROM house_types", cb)
    },
    getHouseType: (id, cb) => {
        return db.query("SELECT * FROM house_types WHERE id=?", [id], cb)
    },
    addHouseType: (input, cb) => {

       console.log(input);
        return db.query("INSERT INTO house_types SET ?", [input], cb)
    },
    updateHouseType: (input, cb) => {
        
        return db.query("UPDATE house_types SET ? WHERE id=?", [input, input.id], cb)
    },
    deleteHouseType: (id, cb) => {
        return db.query("DELETE FROM house_types WHERE id=?", [id], cb);
    }
}

module.exports = model;
