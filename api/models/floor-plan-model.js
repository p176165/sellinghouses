var db = require("../db");
var dateFormat = require('dateformat')

let model = {
    getFloor_plans: (cb) => {
        return db.query("SELECT * FROM floor_plans", cb)
    },
    getFloor_plan: (id, cb) => {
        return db.query("SELECT * FROM floor_plans WHERE id=?", [id], cb)
    },
    addFloor_plan: (input, cb) => {

       console.log(input);
        return db.query("INSERT INTO floor_plans SET ?", [input], cb)
    },
    updateFloor_plan: (input, cb) => {
        
        return db.query("UPDATE floor_plans SET ? WHERE id=?", [input, input.id], cb)
    },
    deleteFloor_plan: (id, cb) => {
        return db.query("DELETE FROM floor_plans WHERE id=?", [id], cb);
    },
    getFloor_planF: (id, cb) => {
        return db.query("SELECT * FROM floor_plans WHERE housetype=?", [id], cb)
    }
}

module.exports = model;