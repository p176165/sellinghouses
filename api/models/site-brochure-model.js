var db = require("../db");
var dateFormat = require('dateformat')

let model = {
    getSite_brochures: (cb) => {
        return db.query("SELECT * FROM site_brochures", cb)
    },
    getSite_brochure: (id, cb) => {
        return db.query("SELECT * FROM site_brochures WHERE id=?", [id], cb)
    },
    addSite_brochure: (input, cb) => {

       console.log(input);
        return db.query("INSERT INTO site_brochures SET ?", [input], cb)
    },
    updateSite_brochure: (input, cb) => {
        
        return db.query("UPDATE site_brochures SET ? WHERE id=?", [input, input.id], cb)
    },
    deleteSite_brochure: (id, cb) => {
        return db.query("DELETE FROM site_brochures WHERE id=?", [id], cb);
    },
    getSite_brochureF: (id, cb) => {
        return db.query("SELECT * FROM site_brochures WHERE development=?", [id], cb)
    }
}

module.exports = model;