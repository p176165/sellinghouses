var db = require("../db");
var dateFormat = require('dateformat')

let model = {
    getDevelopments: (cb) => {
        return db.query("SELECT * FROM developments", cb)
    },
    getDevelopment: (id, cb) => {
        return db.query("SELECT * FROM developments WHERE id=?", [id], cb)
    },
    addDevelopment: (input, cb) => {

       console.log(input);
        return db.query("INSERT INTO developments SET ?", [input], cb)
    },
    updateDevelopment: (input, cb) => {
        
        return db.query("UPDATE developments SET ? WHERE id=?", [input, input.id], cb)
    },
    deleteDevelopment: (id, cb) => {
        return db.query("DELETE FROM developments WHERE id=?", [id], cb);
    }
}

module.exports = model;
