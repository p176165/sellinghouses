
var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var bodyParser = require('body-parser')


require('./passport-config');



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var employeesRouter = require('./routes/employees');
var mapRouter = require('./routes/map');
var developmentsRouter = require('./routes/developments');
var SiteBrochureRouter = require('./routes/SiteBrochure');
var SitePlansRouter = require('./routes/SitePlans');
var HouseTypeRouter = require('./routes/housetype');
var FloorPlanRouter = require('./routes/floorplan');



var app = express(); 

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(bodyParser.json());


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);

app.use('/employees', passport.authenticate('jwt', { session: false }), employeesRouter);
app.use('/map',passport.authenticate('jwt', { session: false }),mapRouter);
app.use('/developments',developmentsRouter);
app.use('/SitePlans',SitePlansRouter);
app.use('/SiteBrochures',SiteBrochureRouter);
app.use('/housetype',HouseTypeRouter);
app.use('/floorplan',FloorPlanRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

