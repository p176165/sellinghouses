var express = require('express');
var router = express.Router();
var db = require("../db");
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({
    Project: 'Monitoring & Tracking System for Mine Safety and Productivity',
    Member01:'Mubashar Ahmed',
    Member02:'Hamza Abid',
    Member03:'Awais Ahmed'
});
});

module.exports = router;
