const express = require('express');
const multer = require('multer');
var router = express.Router();


const DIR = './uploads';
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null, `map.jpg`);
    }
});

const upload = multer({storage: storage});


router.post('/upload', upload.single('photo'), (req, res, next) => {
  const file = req.file;
  console.log(req.body);
  console.log(file.filename);
  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  
    res.send({"status": "success"});
})


  
module.exports = router;
