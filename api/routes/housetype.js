var express = require('express');
const multer = require('multer');
var model = require('../models/house-type');
var router = express.Router();


const DIR = '../src/assets/houseTypes';
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null,  Date.now() + '-' + file.originalname  );
    }
});

const upload = multer({storage: storage});

router.get('/', function(req, res) {
  model.getHouseTypes(function(err, result) {
      if(err) {
          res.json(err)
      } else {
          res.json(result);
      }
  })
})

router.get('/:id', function(req, res) {
  let id = req.params.id;
  model.getHouseType(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})



router.post('/add', upload.single('photo'), (req, res, next) => {
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    
    

    name: req.body.name,
    location: req.body.location,
    description: req.body.description,
    pricing: req.body.pricing,
    bedrooms: req.body.bedrooms,
    total_size: req.body.total_size,
    living_area:req.body.living_area,
    bathrooms:req.body.bathrooms,
    floor_space:req.body.floor_space,
    development: req.body.development,
    kitchen:req.body.kitchen,
    estimated_launch: req.body.estimated_launch,
    image: file.filename

}
  model.addHouseType(data, function(err, result) {
    res.send({"status": "success",data: result, error: err});
})
    
})


router.put('/update', upload.single('photo'), (req, res, next) =>{
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    id:parseInt(req.body.id),
    name: req.body.name,
    location: req.body.location,
    description: req.body.description,
    pricing: req.body.pricing,
    bedrooms: req.body.bedrooms,
    total_size: req.body.total_size,
    living_area:req.body.living_area,
    bathrooms:req.body.bathrooms,
    floor_space:req.body.floor_space,
    development: req.body.development,
    kitchen:req.body.kitchen,
    estimated_launch: req.body.estimated_launch,
    image: file.filename
  }
  console.log(data);
  model.updateHouseType(data, function(err, result) {
      res.json({data: result, error: err});
  })

})


router.delete('/delete/:id', function(req, res) {
  let id = req.params.id;
  model.deleteHouseType(id, function(err, result) {
      res.json({data: result, error: err});
  })
})


module.exports = router;
