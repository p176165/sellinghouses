var express = require('express');
const multer = require('multer');
var model = require('../models/site-brochure-model');
var router = express.Router();


const DIR = '../src/assets/brochures';
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null,  Date.now() + '-' + file.originalname  );
    }
});

const upload = multer({storage: storage});

router.get('/', function(req, res) {
  model.getSite_brochures(function(err, result) {
      if(err) {
          res.json(err)
      } else {
          res.json(result);
      }
  })
})

router.get('/:id', function(req, res) {
  let id = req.params.id;
  model.getSite_brochure(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})

router.get('/brochure/:id', function(req, res) {
  let id = req.params.id;
  model.getSite_brochureF(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})



router.post('/add', upload.single('photo'), (req, res, next) => {
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    
    brochurename: req.body.name,
    development: req.body.development,
    brochurefile: file.filename

}
  model.addSite_brochure(data, function(err, result) {
    res.send({"status": "success",data: result, error: err});
})
    
})


router.put('/update', upload.single('photo'), (req, res, next) =>{
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    id:parseInt(req.body.id),
    brochurename: req.body.name,
    development: req.body.development,
    brochurefile: file.filename

}
  console.log(data);
  model.updateSite_brochure(data, function(err, result) {
      res.json({data: result, error: err});
  })

})


router.delete('/delete/:id', function(req, res) {
  let id = req.params.id;
  model.deleteSite_brochure(id, function(err, result) {
      res.json({data: result, error: err});
  })
})


module.exports = router;
