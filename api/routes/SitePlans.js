var express = require('express');
const multer = require('multer');
var model = require('../models/Site-plan-model');
var router = express.Router();


const DIR = '../src/assets/SitePlans';
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null,  Date.now() + '-' + file.originalname  );
    }
});

const upload = multer({storage: storage});

router.get('/', function(req, res) {
  model.getSite_plans(function(err, result) {
      if(err) {
          res.json(err)
      } else {
          res.json(result);
      }
  })
})

router.get('/:id', function(req, res) {
  let id = req.params.id;
  model.getSite_plan(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})

router.get('/plan/:id', function(req, res) {
  let id = req.params.id;
  model.getSite_planF(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})



router.post('/add', upload.single('photo'), (req, res, next) => {
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    
    sitename: req.body.name,
    development: req.body.development,
    sitefile: file.filename

}
  model.addSite_plan(data, function(err, result) {
    res.send({"status": "success",data: result, error: err});
})
    
})


router.put('/update', upload.single('photo'), (req, res, next) =>{
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    id:parseInt(req.body.id),
    sitename: req.body.name,
    development: req.body.development,
    sitefile: file.filename

}
  console.log(data);
  model.updateSite_plan(data, function(err, result) {
      res.json({data: result, error: err});
  })

})


router.delete('/delete/:id', function(req, res) {
  let id = req.params.id;
  model.deleteSite_plan(id, function(err, result) {
      res.json({data: result, error: err});
  })
})


module.exports = router;
