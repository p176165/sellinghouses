var express = require('express');
const multer = require('multer');
var model = require('../models/development-model');
var router = express.Router();


const DIR = '../src/assets/developments';
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null,  Date.now() + '-' + file.originalname  );
    }
});

const upload = multer({storage: storage});

router.get('/', function(req, res) {
  model.getDevelopments(function(err, result) {
      if(err) {
          res.json(err)
      } else {
          res.json(result);
      }
  })
})

router.get('/:id', function(req, res) {
  let id = req.params.id;
  model.getDevelopment(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})



router.post('/add', upload.single('photo'), (req, res, next) => {
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    
    name: req.body.name,
    location: req.body.location,
    description: req.body.description,
    pricing: req.body.pricing,
    bedrooms: req.body.bedrooms,
    development_size: req.body.development_size,
    estimated_launch: req.body.estimated_launch,
    image: file.filename

}
  model.addDevelopment(data, function(err, result) {
    res.send({"status": "success",data: result, error: err});
})
    
})


router.put('/update', upload.single('photo'), (req, res, next) =>{
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    id:parseInt(req.body.id),
    name: req.body.name,
    location: req.body.location,
    description: req.body.description,
    pricing: req.body.pricing,
    bedrooms: req.body.bedrooms,
    development_size: req.body.development_size,
    estimated_launch: req.body.estimated_launch,
    image: file.filename

  }
  console.log(data);
  model.updateDevelopment(data, function(err, result) {
      res.json({data: result, error: err});
  })

})


router.delete('/delete/:id', function(req, res) {
  let id = req.params.id;
  model.deleteDevelopment(id, function(err, result) {
      res.json({data: result, error: err});
  })
})


module.exports = router;
