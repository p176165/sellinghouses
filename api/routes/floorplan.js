var express = require('express');
const multer = require('multer');
var model = require('../models/floor-plan-model');
var router = express.Router();


const DIR = '../src/assets/FloorPlans';
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null,  Date.now() + '-' + file.originalname  );
    }
});

const upload = multer({storage: storage});

router.get('/', function(req, res) {
  model.getFloor_plans(function(err, result) {
      if(err) {
          res.json(err)
      } else {
          res.json(result);
      }
  })
})

router.get('/:id', function(req, res) {
  let id = req.params.id;
  model.getFloor_plan(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})

router.get('/plan/:id', function(req, res) {
  let id = req.params.id;
  model.getFloor_planF(id, function(err, result) {
      res.json({data: result[0], error: err});
  })
})



router.post('/add', upload.single('photo'), (req, res, next) => {
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    
    floorname: req.body.name,
    housetype: req.body.housetype,
    floorfile: file.filename

}
  model.addFloor_plan(data, function(err, result) {
    res.send({"status": "success",data: result, error: err});
})
    
})


router.put('/update', upload.single('photo'), (req, res, next) =>{
  const file = req.file;

  if (!file) {
    const error = new Error('No File')
    error.httpStatusCode = 400
    return next(error)
  }
  let data = {
    id:parseInt(req.body.id),
    floorname: req.body.name,
    housetype: req.body.housetype,
    floorfile: file.filename

}
  console.log(data);
  model.updateFloor_plan(data, function(err, result) {
      res.json({data: result, error: err});
  })

})


router.delete('/delete/:id', function(req, res) {
  let id = req.params.id;
  model.deleteFloor_plan(id, function(err, result) {
      res.json({data: result, error: err});
  })
})


module.exports = router;
