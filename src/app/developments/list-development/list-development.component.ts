import { Component, OnInit } from '@angular/core';
import { DevelopmentService } from '../services/development.service';
import { Development } from '../models/development';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-development',
  templateUrl: './list-development.component.html',
  styleUrls: ['./list-development.component.scss']
})
export class ListDevelopmentComponent implements OnInit {

  title: string;
  rows: Development[] = [];

  constructor(private developmentService: DevelopmentService,
    private router: Router) { }

  ngOnInit(): void {
    this.title = 'Development';
    this.getDevelopments();
  }

  getDevelopments() {
    this.developmentService.getDevelopments().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

  deleteDevelopment(id: number) {
    if(confirm('Are you sure want to delete?')) {
      this.developmentService.deleteDevelopment(id).subscribe(
        result => {
          console.log(result);
          if ( ! result.error) {
            this.rows = this.rows.filter(item => item.id != id)
          } else {
            alert('Some thingh went wrong!');
          }
        }
      )
    }
  }
}
 