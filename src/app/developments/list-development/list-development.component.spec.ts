import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDevelopmentComponent } from './list-development.component';

describe('ListDevelopmentComponent', () => {
  let component: ListDevelopmentComponent;
  let fixture: ComponentFixture<ListDevelopmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListDevelopmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
