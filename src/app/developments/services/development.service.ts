import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService, HandleError } from '../../services/http-error-handler.service';
import { environment } from '../../../environments/environment';
import { Development } from '../models/development';
@Injectable({
  providedIn: 'root'
})
export class DevelopmentService {

  private apiUrl = `${environment.apiUrl}/developments`;
  private handleError: HandleError;

  httpOptions = {
    headers: new HttpHeaders({
      
    })
  };
 
  redirectUrl: string;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = this.httpErrorHandler.createHandleError('DevelopmentService')
  }

  getDevelopments(): Observable<Development[]> {
    return this.http.get<Development[]>(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError('getDevelopments', []))
    )
  }

  getDevelopment(id: number) {
    return this.http.get<Development>(`${this.apiUrl}/${id}`)
    .pipe(
      catchError(this.handleError('getDevelopment', null))
    )
  }

  addDevelopment(formData) {
    
    return this.http.post<any>(`${this.apiUrl}/add`, formData, this.httpOptions)
    .pipe(
      catchError(this.handleError('addDevelopment', null))
    )
  }

  updateDevelopment(product: Development) {
    return this.http.put<Development>(`${this.apiUrl}/update`, product, this.httpOptions)
    .pipe(
      catchError(this.handleError('updateDevelopment', null))
    )
  }

  deleteDevelopment(id: number) {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
    .pipe(
      catchError(this.handleError('deleteDevelopment', null))
    )
  }

}
