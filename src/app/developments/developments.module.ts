import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { DevelopmentsRoutingModule } from './developments-routing.module';
import { AddDevelopmentComponent } from './add-development/add-development.component';
import { ListDevelopmentComponent } from './list-development/list-development.component';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';

@NgModule({
  declarations: [AddDevelopmentComponent, ListDevelopmentComponent,FileUploaderComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DevelopmentsRoutingModule
  ]
})
export class DevelopmentsModule { }
