import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ListDevelopmentComponent } from './list-development/list-development.component';
import { AddDevelopmentComponent } from './add-development/add-development.component'; 

const routes: Routes = [ 
  {
  path: '',
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    {
      path: '',
      component: ListDevelopmentComponent
    },
    {
      path: 'create',
      component: AddDevelopmentComponent
    },
    {
      path: 'edit/:id',
      component: AddDevelopmentComponent 
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevelopmentsRoutingModule { }
