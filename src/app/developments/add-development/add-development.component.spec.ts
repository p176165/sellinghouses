import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDevelopmentComponent } from './add-development.component';

describe('AddDevelopmentComponent', () => {
  let component: AddDevelopmentComponent;
  let fixture: ComponentFixture<AddDevelopmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDevelopmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
