import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DevelopmentService } from '../services/development.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Development } from '../models/development';
@Component({
  selector: 'app-add-development',
  templateUrl: './add-development.component.html',
  styleUrls: ['./add-development.component.scss']
})
export class AddDevelopmentComponent implements OnInit {

  userForm: FormGroup;
  isPhotoError = false;
  image: string;
  submitted : boolean = false;
  uploadError: string = '';

  model: Development;
  title: string; 
  developmentId: number; 
  

  constructor(private fb: FormBuilder,private developmentService:DevelopmentService,
    private route: ActivatedRoute,
    private router: Router) {
   }

  ngOnInit(): void {
    this.title = "Create Development";
    this.newForm();
    this.developmentId = +this.route.snapshot.paramMap.get('id');
    if(this.developmentId) {
      this.getDevelopment();
    }
  }

  goBack() {
    this.router.navigateByUrl('/backend/development');
  }

  newForm = function () {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      location: ['', Validators.required],
      pricing: ['', Validators.required],
      bedrooms: ['', Validators.required],
      development_size: ['', Validators.required],
      estimated_launch: ['', Validators.required],
      description: ['', Validators.required],
      photo: ['', Validators.compose([Validators.required])]
    })
  }

  
  PostData() {
    this.submitted = true;
    if(!this.userForm.valid) {
      return false;
    }
    if (this.userForm.get('photo').invalid) {
      this.isPhotoError = true;
    }
    this.uploadError = '';
    const formData = new FormData();
    formData.append('name',this.userForm.get('name').value);
    formData.append('location',this.userForm.get('location').value);
    formData.append('pricing',this.userForm.get('pricing').value);
    formData.append('bedrooms',this.userForm.get('bedrooms').value);
    formData.append('development_size',this.userForm.get('development_size').value);
    formData.append('estimated_launch',this.userForm.get('estimated_launch').value);
    formData.append('description',this.userForm.get('description').value);
    formData.append('photo', this.userForm.get('photo').value);
    if (this.developmentId) {
      this.updateDevelopment(formData);
    } else {
      this.developmentService.addDevelopment(formData).subscribe(
        resp => {
          if(resp['status'] == 'success') {
            alert('Data is saved into database');
            this.router.navigateByUrl('/backend/developments');
          }
        }, (resp)=> {
          this.uploadError = 'Some error occured please try later';
          console.log(resp);
        }
      )
    }
    
  }

  onFileSelect(file: Event) {
    this.userForm.patchValue({ photo: file });
    this.userForm.get('photo').updateValueAndValidity();
  }

  

  updateDevelopment(formdata:any) {
    formdata.append('id', this.developmentId);
    this.developmentService.updateDevelopment(formdata).subscribe(
      result => {
        console.log(result);
        if ( ! result.error) {
          this.router.navigateByUrl('/backend/developments');
        } else {
          alert('Some thingh went wrong!');
        }
      }
    )
  }

  getDevelopment() {
    this.developmentService.getDevelopment(this.developmentId).subscribe(
      result => {
        console.log(result);
        this.userForm.patchValue(result.data)
      }
    )
  }

}
