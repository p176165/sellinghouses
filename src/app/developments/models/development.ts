export interface Development {
    id:number;
    name:string;
    location:string;
    description:string;
    pricing:string;
    bedrooms:string;
    development_size:string;
    estimated_launch:string;
    image:string;
}
