import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSiteBrochureComponent } from './list-site-brochure.component';

describe('ListSiteBrochureComponent', () => {
  let component: ListSiteBrochureComponent;
  let fixture: ComponentFixture<ListSiteBrochureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSiteBrochureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSiteBrochureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
