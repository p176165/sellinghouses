import { Component, OnInit } from '@angular/core';
import { SiteBrochureService } from '../services/site-brochure.service';
import { SiteBrochure } from '../models/site-brochure';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-site-brochure',
  templateUrl: './list-site-brochure.component.html',
  styleUrls: ['./list-site-brochure.component.scss']
})
export class ListSiteBrochureComponent implements OnInit {

  title: string;
  rows: SiteBrochure[] = [];

  constructor(private siteBrochureService: SiteBrochureService,
    private router: Router) { }

  ngOnInit(): void {
    this.title = 'Site Brochure';
    this.getSitePlans();
  }

  getSitePlans() {
    this.siteBrochureService.getSiteBrochures().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

  deleteSitePlan(id: number) {
    if(confirm('Are you sure want to delete?')) {
      this.siteBrochureService.deleteSiteBrochure(id).subscribe(
        result => {
          console.log(result);
          if ( ! result.error) {
            this.rows = this.rows.filter(item => item.id != id)
          } else {
            alert('Some thingh went wrong!');
          }
        }
      )
    }
  }
}
 