import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService, HandleError } from '../../services/http-error-handler.service';
import { environment } from '../../../environments/environment';
import { SiteBrochure } from '../models/site-brochure';

@Injectable({
  providedIn: 'root'
})
export class SiteBrochureService {

  private apiUrl = `${environment.apiUrl}/SiteBrochures`;
  private handleError: HandleError; 

  httpOptions = {
    headers: new HttpHeaders({
      
    })
  };
 
  redirectUrl: string;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = this.httpErrorHandler.createHandleError('SitePlanService')
  }

  getSiteBrochures(): Observable<SiteBrochure[]> {
    return this.http.get<SiteBrochure[]>(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError('getSiteBrochures', []))
    )
  }

  getSiteBrochure(id: number) {
    return this.http.get<SiteBrochure>(`${this.apiUrl}/${id}`)
    .pipe(
      catchError(this.handleError('getSiteBrochure', null))
    )
  }

  getSiteBrochureF(id: number) {
    return this.http.get<SiteBrochure>(`${this.apiUrl}/brochure/${id}`)
    .pipe(
      catchError(this.handleError('getSiteBrochureF', null))
    )
  }

  addSiteBrochure(formData) {
    
    return this.http.post<any>(`${this.apiUrl}/add`, formData, this.httpOptions)
    .pipe(
      catchError(this.handleError('addSiteBrochure', null))
    )
  }

  updateSiteBrochure(product: SiteBrochure) {
    return this.http.put<SiteBrochure>(`${this.apiUrl}/update`, product, this.httpOptions)
    .pipe(
      catchError(this.handleError('updateSiteBrochure', null))
    )
  }

  deleteSiteBrochure(id: number) {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
    .pipe(
      catchError(this.handleError('deleteSiteBrochure', null))
    )
  }
}
