import { TestBed } from '@angular/core/testing';

import { SiteBrochureService } from './site-brochure.service';

describe('SiteBrochureService', () => {
  let service: SiteBrochureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SiteBrochureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
