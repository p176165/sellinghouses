export interface SiteBrochure {
    id:number;
    brochurename:string;
    brochurefile:string;
    development:number;
}
