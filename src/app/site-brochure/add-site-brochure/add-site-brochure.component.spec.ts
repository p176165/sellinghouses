import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSiteBrochureComponent } from './add-site-brochure.component';

describe('AddSiteBrochureComponent', () => {
  let component: AddSiteBrochureComponent;
  let fixture: ComponentFixture<AddSiteBrochureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSiteBrochureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSiteBrochureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
