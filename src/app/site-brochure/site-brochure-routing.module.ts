import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ListSiteBrochureComponent } from '../site-brochure/list-site-brochure/list-site-brochure.component';
import { AddSiteBrochureComponent}  from '../site-brochure/add-site-brochure/add-site-brochure.component'
const routes: Routes = [ 
  {
  path: '',
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    {
      path: '',
      component: ListSiteBrochureComponent
    },
    {
      path: 'create',
      component: AddSiteBrochureComponent
    },
    {
      path: 'edit/:id',
      component: AddSiteBrochureComponent 
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteBrochureRoutingModule { }
