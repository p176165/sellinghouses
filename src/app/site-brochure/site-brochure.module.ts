import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { SiteBrochureRoutingModule } from './site-brochure-routing.module';
import { AddSiteBrochureComponent } from './add-site-brochure/add-site-brochure.component';
import { ListSiteBrochureComponent } from './list-site-brochure/list-site-brochure.component';
import { FileUploaderComponent} from './file-uploader/file-uploader.component'

@NgModule({
  declarations: [AddSiteBrochureComponent, ListSiteBrochureComponent,FileUploaderComponent],
  imports: [
    CommonModule,
    SiteBrochureRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SiteBrochureModule { }
