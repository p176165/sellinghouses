import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { SitePlanRoutingModule } from './site-plan-routing.module';
import { AddSitePlanComponent } from './add-site-plan/add-site-plan.component';
import { ListSitePlanComponent } from './list-site-plan/list-site-plan.component';
import { FileUploaderComponent} from './file-uploader/file-uploader.component'

@NgModule({
  declarations: [AddSitePlanComponent, ListSitePlanComponent,FileUploaderComponent],
  imports: [
    CommonModule,
    SitePlanRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SitePlanModule { }
