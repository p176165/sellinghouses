import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ListSitePlanComponent } from './list-site-plan/list-site-plan.component';
import { AddSitePlanComponent } from './add-site-plan/add-site-plan.component'; 

const routes: Routes = [ 
  {
  path: '',
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    {
      path: '',
      component: ListSitePlanComponent
    },
    {
      path: 'create',
      component: AddSitePlanComponent
    },
    {
      path: 'edit/:id',
      component: AddSitePlanComponent 
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SitePlanRoutingModule { }
