import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSitePlanComponent } from './list-site-plan.component';

describe('ListSitePlanComponent', () => {
  let component: ListSitePlanComponent;
  let fixture: ComponentFixture<ListSitePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSitePlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSitePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
