import { Component, OnInit } from '@angular/core';
import { SitePlanService } from '../services/site-plan.service';
import { SitePlan } from '../models/site-plan';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-site-plan',
  templateUrl: './list-site-plan.component.html',
  styleUrls: ['./list-site-plan.component.scss']
})
export class ListSitePlanComponent implements OnInit {

  title: string;
  rows: SitePlan[] = [];

  constructor(private sitePlanService: SitePlanService,
    private router: Router) { }

  ngOnInit(): void {
    this.title = 'Site plans';
    this.getSitePlans();
  }

  getSitePlans() {
    this.sitePlanService.getSitePlans().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

  deleteSitePlan(id: number) {
    if(confirm('Are you sure want to delete?')) {
      this.sitePlanService.deleteSitePlan(id).subscribe(
        result => {
          console.log(result);
          if ( ! result.error) {
            this.rows = this.rows.filter(item => item.id != id)
          } else {
            alert('Some thingh went wrong!');
          }
        }
      )
    }
  }

}
