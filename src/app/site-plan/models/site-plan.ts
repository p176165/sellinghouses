export interface SitePlan {
    id:number;
    sitename:string;
    sitefile:string;
    development:number;
}
