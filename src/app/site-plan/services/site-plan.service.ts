import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService, HandleError } from '../../services/http-error-handler.service';
import { environment } from '../../../environments/environment';
import { SitePlan } from '../models/site-plan';

@Injectable({
  providedIn: 'root'
})
export class SitePlanService {

  private apiUrl = `${environment.apiUrl}/SitePlans`;
  private handleError: HandleError; 

  httpOptions = {
    headers: new HttpHeaders({
      
    })
  };
 
  redirectUrl: string;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = this.httpErrorHandler.createHandleError('SitePlanService')
  }

  getSitePlans(): Observable<SitePlan[]> {
    return this.http.get<SitePlan[]>(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError('getSitePlans', []))
    )
  }

  getSitePlan(id: number) {
    return this.http.get<SitePlan>(`${this.apiUrl}/${id}`)
    .pipe(
      catchError(this.handleError('getSitePlan', null))
    )
  }

  getSitePlanF(id: number) {
    return this.http.get<SitePlan>(`${this.apiUrl}/plan/${id}`)
    .pipe(
      catchError(this.handleError('getSitePlanF', null))
    )
  }


  addSitePlan(formData) {
    
    return this.http.post<any>(`${this.apiUrl}/add`, formData, this.httpOptions)
    .pipe(
      catchError(this.handleError('addSitePlan', null))
    )
  }

  updateSitePlan(product: SitePlan) {
    return this.http.put<SitePlan>(`${this.apiUrl}/update`, product, this.httpOptions)
    .pipe(
      catchError(this.handleError('updateSitePlan', null))
    )
  }

  deleteSitePlan(id: number) {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
    .pipe(
      catchError(this.handleError('deleteSitePlan', null))
    )
  }
}
