import { TestBed } from '@angular/core/testing';

import { SitePlanService } from './site-plan.service';

describe('SitePlanService', () => {
  let service: SitePlanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SitePlanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
