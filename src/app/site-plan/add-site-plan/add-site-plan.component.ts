import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { DevelopmentService } from '../../developments/services/development.service';
import { SitePlanService } from '../services/site-plan.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Development } from '../../developments/models/development';
import { SitePlan } from '../models/site-plan';


@Component({
  selector: 'app-add-site-plan',
  templateUrl: './add-site-plan.component.html',
  styleUrls: ['./add-site-plan.component.scss']
})
export class AddSitePlanComponent implements OnInit {

  userForm: FormGroup;
  isPhotoError = false;
  image: string;
  submitted : boolean = false;
  uploadError: string = '';

  developments:any;
  
  title: string; 
  siteId: number; 
  constructor(private fb: FormBuilder,private developmentService:DevelopmentService,
    private route: ActivatedRoute,
    private router: Router,private sitePlanService :SitePlanService) { }

  ngOnInit(): void {
    this.title = "Create Development Site Plan";
    this.newForm();
    this.getdevelopments();
    this.siteId = +this.route.snapshot.paramMap.get('id');
    if(this.siteId) {
      this.getSitePlan();
    }
  }

  goBack() {
    this.router.navigateByUrl('/backend/site-plan');
  }

  newForm = function () {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      development: ['', Validators.required],
      photo: ['', Validators.compose([Validators.required])]
    })
  }

  
  PostData() {
    this.submitted = true;
    if(!this.userForm.valid) {
      return false;
    }
    if (this.userForm.get('photo').invalid) {
      this.isPhotoError = true;
    }
    this.uploadError = '';
    const formData = new FormData();
    formData.append('name',this.userForm.get('name').value);
    formData.append('development',this.userForm.get('development').value);
    formData.append('photo', this.userForm.get('photo').value);
    if (this.siteId) {
      this.updateDevelopment(formData);
    } else {
      this.sitePlanService.addSitePlan(formData).subscribe(
        resp => {
          if(resp['status'] == 'success') {
            alert('Data is saved into database');
            this.router.navigateByUrl('/backend/site-plan');
          }
        }, (resp)=> {
          this.uploadError = 'Some error occured please try later';
          console.log(resp);
        }
      )
    }
    
  }

  onFileSelect(file: Event) {
    this.userForm.patchValue({ photo: file });
    this.userForm.get('photo').updateValueAndValidity();
  }

  

  updateDevelopment(formdata:any) {
    formdata.append('id', this.siteId);
    this.sitePlanService.updateSitePlan(formdata).subscribe(
      result => {
        console.log(result);
        if ( ! result.error) {
          this.router.navigateByUrl('/backend/site-plan');
        } else {
          alert('Some thingh went wrong!');
        }
      }
    )
  }

  getSitePlan() {
    this.sitePlanService.getSitePlan(this.siteId).subscribe(
      result => {
        console.log(result);
        this.userForm.patchValue(result.data)
      }
    )
  }
  getdevelopments() {
    this.developmentService.getDevelopments().subscribe(
      result => {
        console.log(result[0]);
        this.developments = result
      }
    )
  }

}
