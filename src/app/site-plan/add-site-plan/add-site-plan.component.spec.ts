import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSitePlanComponent } from './add-site-plan.component';

describe('AddSitePlanComponent', () => {
  let component: AddSitePlanComponent;
  let fixture: ComponentFixture<AddSitePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSitePlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSitePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
