
import { Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';

export const BACKEND_LAYOUT: Routes = [
    
    
    {
        path: 'developments',
        loadChildren: () => import('../developments/developments.module').then(m => m.DevelopmentsModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'site-plan',
        loadChildren: () => import('../site-plan/site-plan.module').then(m => m.SitePlanModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'site-brochure',
        loadChildren: () => import('../site-brochure/site-brochure.module').then(m => m.SiteBrochureModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'house-type',
        loadChildren: () => import('../house-type/house-type.module').then(m => m.HouseTypeModule),
        canLoad: [AuthGuard]
    },
    {
        path: 'floor-plan',
        loadChildren: () => import('../floor-plan/floor-plan.module').then(m => m.FloorPlanModule),
        canLoad: [AuthGuard]
    }
]
