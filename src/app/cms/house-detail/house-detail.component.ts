
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HouseTypeService } from '../../house-type/services/house-type.service';
import { HouseType } from '../../house-type/models/house-type';
import { FloorPlanService} from '../../floor-plan/services/floor-plan.service';

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.scss']
})
export class HouseDetailComponent implements OnInit {
  customCss : any;
  floorPlanCss : any;
  data: HouseType;
  houseData:any;
  title: string;
  

  housetypeId: number; 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private houseTypeService:HouseTypeService,
    private floorPlanService:FloorPlanService) { }

  ngOnInit(): void {
    this.housetypeId = +this.route.snapshot.paramMap.get('id');
    if(this.housetypeId) {
      this.getHouseType();
      this.getFloorPlanF();
    }
  }

  getHouseType() {
    this.houseTypeService.getHouseType(this.housetypeId).subscribe(
      result => {
        this.customCss = `assets/houseTypes/${result.data.image}`;
        
        this.data = result.data;
      }
    )
  }



  getFloorPlanF() {
    this.floorPlanService.getFloorPlanF(this.housetypeId).subscribe(
      result => {
        this.floorPlanCss = result.data.floorfile;
      }
    )
  }

  


} 





