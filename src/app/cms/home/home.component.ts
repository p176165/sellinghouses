import { Component, OnInit } from '@angular/core';
import { DevelopmentService } from '../../developments/services/development.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Development } from '../../developments/models/development';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  
  rows: Development[] = [];

  constructor(private developmentService: DevelopmentService,
    private router: Router) { }

  ngOnInit() {
    
    
    this.getDevelopments();

  }
  
  getDevelopments() {
    this.developmentService.getDevelopments().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

}
