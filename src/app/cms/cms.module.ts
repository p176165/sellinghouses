import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CmsRoutingModule } from './cms-routing.module';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DevelopmentsComponent } from './developments/developments.component';
import { DevelopmentDetailComponent } from './development-detail/development-detail.component';
import { HouseDetailComponent } from './house-detail/house-detail.component';


@NgModule({
  declarations: [HomeComponent, PageNotFoundComponent, AboutComponent, ContactComponent, DevelopmentsComponent, DevelopmentDetailComponent, HouseDetailComponent],
  imports: [
    CommonModule,
    CmsRoutingModule
  ]
})
export class CmsModule { }
