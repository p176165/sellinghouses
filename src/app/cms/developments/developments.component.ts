import { Component, OnInit } from '@angular/core';
import { DevelopmentService } from '../../developments/services/development.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Development } from '../../developments/models/development';

@Component({
  selector: 'app-developments',
  templateUrl: './developments.component.html',
  styleUrls: ['./developments.component.scss']
})
export class DevelopmentsComponent implements OnInit {

  rows: Development[] = [];

  constructor(private developmentService: DevelopmentService,
    private router: Router) { }

  ngOnInit(): void {

    this.getDevelopments();
  }

  getDevelopments() {
    this.developmentService.getDevelopments().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

}


