import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopmentDetailComponent } from './development-detail.component';

describe('DevelopmentDetailComponent', () => {
  let component: DevelopmentDetailComponent;
  let fixture: ComponentFixture<DevelopmentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevelopmentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
