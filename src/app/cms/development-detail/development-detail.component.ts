
import { Component, OnInit } from '@angular/core';
import { DevelopmentService } from '../../developments/services/development.service';
import { SitePlanService} from '../../site-plan/services/site-plan.service';
import { SiteBrochureService } from '../../site-brochure/services/site-brochure.service'
import { ActivatedRoute, Router } from '@angular/router';
import { Development } from '../../developments/models/development';
import { HouseTypeService } from '../../house-type/services/house-type.service';
import { HouseType } from '../../house-type/models/house-type';

@Component({
  selector: 'app-development-detail',
  templateUrl: './development-detail.component.html',
  styleUrls: ['./development-detail.component.scss']
})
export class DevelopmentDetailComponent implements OnInit {

  customCss : any;
  sitePlanCss : any;
  siteBrochureCss: any;
  siteBrochureName:string;
  data: Development;
  title: string;
  developmentId: number; 
  houseData:any;

  constructor(private developmentService:DevelopmentService,
    private route: ActivatedRoute,
    private router: Router,private sitePlanService: SitePlanService,
    private houseTypeService:HouseTypeService,
    private siteBrochureService: SiteBrochureService) { }

  ngOnInit(): void {
    this.developmentId = +this.route.snapshot.paramMap.get('id');
    if(this.developmentId) {
      this.getDevelopment();
      this.getSitePlanF();
      this.getSiteBrochureF();
      this.getHouseTypes();
      
    }
  }

  getDevelopment() {
    this.developmentService.getDevelopment(this.developmentId).subscribe(
      result => {
        this.customCss = `assets/developments/${result.data.image}`;
        
        this.data = result.data;
      }
    )
  }

  getSitePlanF() {
    this.sitePlanService.getSitePlanF(this.developmentId).subscribe(
      result => {
        this.sitePlanCss = result.data.sitefile;
      }
    )
  }

  getHouseTypes() {
    this.houseTypeService.getHouseTypes().subscribe(
      result => {
        console.log(result);
        this.houseData = result;
      }
    )
  }

  getSiteBrochureF() {
    this.siteBrochureService.getSiteBrochureF(this.developmentId).subscribe(
      result => {
        this.siteBrochureCss = result.data.brochurefile;
        this.siteBrochureName = result.data.brochurename;
        
      }
    )
  }

}
