import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/backend/developments', title: 'Developments',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/backend/site-brochure', title: 'Site Brochure',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/backend/site-plan', title: 'Site Plans',  icon:'ni-planet text-blue', class: '' },
    { path: '/backend/house-type', title: 'House Type',  icon:'ni-planet text-blue', class: '' },
    { path: '/backend/floor-plan', title: 'Floor Plans',  icon:'ni-planet text-blue', class: '' },
   
    
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
