import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { HouseTypeService } from '../../house-type/services/house-type.service';
import { FloorPlanService } from '../services/floor-plan.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HouseType } from '../../house-type/models/house-type';
import { FloorPlan } from '../models/floor-plan';

@Component({
  selector: 'app-add-floor-plan',
  templateUrl: './add-floor-plan.component.html',
  styleUrls: ['./add-floor-plan.component.scss']
})
export class AddFloorPlanComponent implements OnInit {
  userForm: FormGroup;
  isPhotoError = false;
  image: string;
  submitted : boolean = false;
  uploadError: string = '';

  houses:any;
  
  title: string; 
  siteId: number; 

  constructor(private fb: FormBuilder,private houseTypeService:HouseTypeService,
    private route: ActivatedRoute,
    private router: Router,private floorPlanService :FloorPlanService) { }

  ngOnInit(): void {
    this.title = "Create Floor  Plan";
    this.newForm();
    this.getHouses();
    this.siteId = +this.route.snapshot.paramMap.get('id');
    if(this.siteId) {
      this.getFloorPlan();
    }
  }

  goBack() {
    this.router.navigateByUrl('/backend/floor-plan');
  }

  newForm = function () {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      housetype: ['', Validators.required],
      photo: ['', Validators.compose([Validators.required])]
    })
  }

  
  PostData() {
    this.submitted = true;
    if(!this.userForm.valid) {
      return false;
    }
    if (this.userForm.get('photo').invalid) {
      this.isPhotoError = true;
    }
    this.uploadError = '';
    const formData = new FormData();
    formData.append('name',this.userForm.get('name').value);
    formData.append('housetype',this.userForm.get('housetype').value);
    formData.append('photo', this.userForm.get('photo').value);
    if (this.siteId) {
      this.updateFloorType(formData);
    } else {
      this.floorPlanService.addFloorPlan(formData).subscribe(
        resp => {
          if(resp['status'] == 'success') {
            alert('Data is saved into database');
            this.router.navigateByUrl('/backend/floor-plan');
          }
        }, (resp)=> {
          this.uploadError = 'Some error occured please try later';
          console.log(resp);
        }
      )
    }
    
  }

  onFileSelect(file: Event) {
    this.userForm.patchValue({ photo: file });
    this.userForm.get('photo').updateValueAndValidity();
  }

  

  updateFloorType(formdata:any) {
    formdata.append('id', this.siteId);
    this.floorPlanService.updateFloorPlan(formdata).subscribe(
      result => {
        console.log(result);
        if ( ! result.error) {
          this.router.navigateByUrl('/backend/floor-plan');
        } else {
          alert('Some thingh went wrong!');
        }
      }
    )
  }

  getFloorPlan() {
    this.floorPlanService.getFloorPlan(this.siteId).subscribe(
      result => {
        
        this.userForm.patchValue(result.data)
      }
    )
  }
  getHouses() {
    this.houseTypeService.getHouseTypes().subscribe(
      result => {
        
        this.houses = result
        
      }
    )
  }

}
 