import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFloorPlanComponent } from './add-floor-plan.component';

describe('AddFloorPlanComponent', () => {
  let component: AddFloorPlanComponent;
  let fixture: ComponentFixture<AddFloorPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFloorPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFloorPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
