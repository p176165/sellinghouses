import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ListFloorPlanComponent } from './list-floor-plan/list-floor-plan.component';
import { AddFloorPlanComponent } from './add-floor-plan/add-floor-plan.component'; 
const routes: Routes = [
  {
  path: '',
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    {
      path: '',
      component: ListFloorPlanComponent
    },
    {
      path: 'create',
      component: AddFloorPlanComponent
    },
    {
      path: 'edit/:id',
      component: AddFloorPlanComponent 
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FloorPlanRoutingModule { }
