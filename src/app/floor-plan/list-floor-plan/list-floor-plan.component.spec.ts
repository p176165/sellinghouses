import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFloorPlanComponent } from './list-floor-plan.component';

describe('ListFloorPlanComponent', () => {
  let component: ListFloorPlanComponent;
  let fixture: ComponentFixture<ListFloorPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListFloorPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFloorPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
