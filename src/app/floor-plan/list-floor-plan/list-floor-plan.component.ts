import { Component, OnInit } from '@angular/core';
import { FloorPlanService } from '../services/floor-plan.service';
import { FloorPlan } from '../models/floor-plan';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-floor-plan',
  templateUrl: './list-floor-plan.component.html',
  styleUrls: ['./list-floor-plan.component.scss']
})
export class ListFloorPlanComponent implements OnInit {
  title: string;
  rows: FloorPlan[] = [];

  constructor(private floorPlanService: FloorPlanService,
    private router: Router) { }

  ngOnInit(): void {
    this.title = 'Floor plans';
    this.getFloorPlans();
  }

  getFloorPlans() {
    this.floorPlanService.getFloorPlans().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

  deleteFloorPlan(id: number) {
    if(confirm('Are you sure want to delete?')) {
      this.floorPlanService.deleteFloorPlan(id).subscribe(
        result => {
          console.log(result);
          if ( ! result.error) {
            this.rows = this.rows.filter(item => item.id != id)
          } else {
            alert('Some thingh went wrong!');
          }
        }
      )
    }
  }


}
