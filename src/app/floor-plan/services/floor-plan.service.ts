import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService, HandleError } from '../../services/http-error-handler.service';
import { environment } from '../../../environments/environment';
import { FloorPlan } from '../models/floor-plan';

@Injectable({
  providedIn: 'root'
})
export class FloorPlanService {

  private apiUrl = `${environment.apiUrl}/floorplan`;
  private handleError: HandleError; 

  httpOptions = {
    headers: new HttpHeaders({
      
    })
  };
 
  redirectUrl: string;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = this.httpErrorHandler.createHandleError('FloorPlanService')
  }

  getFloorPlanF(id: number) {
    return this.http.get<FloorPlan>(`${this.apiUrl}/plan/${id}`)
    .pipe(
      catchError(this.handleError('getFloorPlanF', null))
    )
  }
  
  getFloorPlans(): Observable<FloorPlan[]> {
    return this.http.get<FloorPlan[]>(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError('getFloorPlans', []))
    )
  }

  getFloorPlan(id: number) {
    return this.http.get<FloorPlan>(`${this.apiUrl}/${id}`)
    .pipe(
      catchError(this.handleError('getFloorPlan', null))
    )
  }

  


  addFloorPlan(formData) {
    
    return this.http.post<any>(`${this.apiUrl}/add`, formData, this.httpOptions)
    .pipe(
      catchError(this.handleError('addFloorPlan', null))
    )
  }

  updateFloorPlan(product: FloorPlan) {
    return this.http.put<FloorPlan>(`${this.apiUrl}/update`, product, this.httpOptions)
    .pipe(
      catchError(this.handleError('updateFloorPlan', null))
    )
  }

  deleteFloorPlan(id: number) {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
    .pipe(
      catchError(this.handleError('deleteFloorPlan', null))
    )
  }
}
 