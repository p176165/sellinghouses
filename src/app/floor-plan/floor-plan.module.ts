import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';


import { FileUploaderComponent} from './file-uploader/file-uploader.component';

import { FloorPlanRoutingModule } from './floor-plan-routing.module';
import { AddFloorPlanComponent } from './add-floor-plan/add-floor-plan.component';
import { ListFloorPlanComponent } from './list-floor-plan/list-floor-plan.component';


@NgModule({
  declarations: [AddFloorPlanComponent, ListFloorPlanComponent,FileUploaderComponent],
  imports: [
    CommonModule,
    FloorPlanRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class FloorPlanModule { }
