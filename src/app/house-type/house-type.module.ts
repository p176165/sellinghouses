import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { HouseTypeRoutingModule } from './house-type-routing.module';
import { AddHouseTypeComponent } from './add-house-type/add-house-type.component';
import { ListHouseTypeComponent } from './list-house-type/list-house-type.component';
import { FileUploaderComponent} from './file-uploader/file-uploader.component'

@NgModule({
  declarations: [AddHouseTypeComponent, ListHouseTypeComponent,FileUploaderComponent],
  imports: [
    CommonModule,
    HouseTypeRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class HouseTypeModule { }
