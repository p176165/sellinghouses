export interface HouseType {
    id:number;
    name:string;
    location:string;
    description:string;
    pricing:string;
    bedrooms:string;
    total_size:string;
    living_area:string;
    bathrooms:string;
    floor_space:string;
    kitchen:string;
    estimated_launch:string;
    image:string;
}
