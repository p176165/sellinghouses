import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { HouseTypeService } from '../services/house-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HouseType } from '../models/house-type';
import { DevelopmentService } from '../../developments/services/development.service';

@Component({
  selector: 'app-add-house-type',
  templateUrl: './add-house-type.component.html',
  styleUrls: ['./add-house-type.component.scss']
})
export class AddHouseTypeComponent implements OnInit {

  userForm: FormGroup;
  isPhotoError = false;
  image: string;
  submitted : boolean = false;
  uploadError: string = '';

  model: HouseType;
  title: string; 
  houseId: number; 
  developments:any;
  

  constructor(private fb: FormBuilder,private developmentService:DevelopmentService,
    private route: ActivatedRoute,
    private router: Router,private houseTypeService:HouseTypeService ) {
   }

  ngOnInit(): void {
    this.title = "Create Development";
    this.newForm();
    this.houseId = +this.route.snapshot.paramMap.get('id');
    this.getdevelopments();
    if(this.houseId) {
      this.getHouseType();
    }
  }

  goBack() {
    this.router.navigateByUrl('/backend/house-type');
  }

  newForm = function () {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      location: ['', Validators.required],
      pricing: ['', Validators.required],
      bedrooms: ['', Validators.required],
      total_size: ['', Validators.required],
      estimated_launch: ['', Validators.required],
      living_area: ['', Validators.required],
      bathrooms: ['', Validators.required],
      development: ['', Validators.required],
      floor_space: ['', Validators.required],
      kitchen: ['', Validators.required],
      description: ['', Validators.required],
      photo: ['', Validators.compose([Validators.required])]
    })
  }

  
  PostData() {
    this.submitted = true;
    if(!this.userForm.valid) {
      return false;
    }
    if (this.userForm.get('photo').invalid) {
      this.isPhotoError = true;
    }
    this.uploadError = '';
    const formData = new FormData();
    formData.append('name',this.userForm.get('name').value);
    formData.append('location',this.userForm.get('location').value);
    formData.append('pricing',this.userForm.get('pricing').value);
    formData.append('bedrooms',this.userForm.get('bedrooms').value);
    formData.append('total_size',this.userForm.get('total_size').value);
    formData.append('development',this.userForm.get('development').value);
    formData.append('estimated_launch',this.userForm.get('estimated_launch').value);
    formData.append('living_area',this.userForm.get('living_area').value);
    formData.append('bathrooms',this.userForm.get('bathrooms').value);
    formData.append('floor_space',this.userForm.get('floor_space').value);
    formData.append('kitchen',this.userForm.get('kitchen').value);
    formData.append('description',this.userForm.get('description').value);
    formData.append('photo', this.userForm.get('photo').value);
    if (this.houseId) {
      this.updateHouseType(formData);
    } else {
      this.houseTypeService.addHouseType(formData).subscribe(
        resp => {
          if(resp['status'] == 'success') {
            alert('Data is saved into database');
            this.router.navigateByUrl('/backend/house-type');
          }
        }, (resp)=> {
          this.uploadError = 'Some error occured please try later';
          console.log(resp);
        }
      )
    }
    
  }

  onFileSelect(file: Event) {
    this.userForm.patchValue({ photo: file });
    this.userForm.get('photo').updateValueAndValidity();
  }

  

  updateHouseType(formdata:any) {
    formdata.append('id', this.houseId);
    this.houseTypeService.updateHouseType(formdata).subscribe(
      result => {
        console.log(result);
        if ( ! result.error) {
          this.router.navigateByUrl('/backend/house-type');
        } else {
          alert('Some thingh went wrong!');
        }
      }
    )
  }

  getHouseType() {
    this.houseTypeService.getHouseType(this.houseId).subscribe(
      result => {
        console.log(result);
        this.userForm.patchValue(result.data)
      }
    )
  }

  getdevelopments() {
    this.developmentService.getDevelopments().subscribe(
      result => {
        this.developments = result
      }
    )
  }
}
