import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService, HandleError } from '../../services/http-error-handler.service';
import { environment } from '../../../environments/environment';
import { HouseType } from '../models/house-type';

@Injectable({
  providedIn: 'root'
})
export class HouseTypeService {
  private apiUrl = `${environment.apiUrl}/housetype`;
  private handleError: HandleError;

  httpOptions = {
    headers: new HttpHeaders({
      
    })
  };
 
  redirectUrl: string;

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = this.httpErrorHandler.createHandleError('HouseTypeService')
  }

  getHouseTypes(): Observable<HouseType[]> {
    return this.http.get<HouseType[]>(`${this.apiUrl}`)
    .pipe(
      catchError(this.handleError('getHouseTypes', []))
    )
  }

  getHouseType(id: number) {
    return this.http.get<HouseType>(`${this.apiUrl}/${id}`)
    .pipe(
      catchError(this.handleError('getHouseType', null))
    )
  }

  addHouseType(formData) {
    
    return this.http.post<any>(`${this.apiUrl}/add`, formData, this.httpOptions)
    .pipe(
      catchError(this.handleError('addHouseType', null))
    )
  }

  updateHouseType(product: HouseType) {
    return this.http.put<HouseType>(`${this.apiUrl}/update`, product, this.httpOptions)
    .pipe(
      catchError(this.handleError('updateHouseType', null))
    )
  }

  deleteHouseType(id: number) {
    return this.http.delete(`${this.apiUrl}/delete/${id}`)
    .pipe(
      catchError(this.handleError('deleteHouseType', null))
    )
  }
}
