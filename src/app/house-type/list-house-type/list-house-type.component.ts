import { Component, OnInit } from '@angular/core';
import { HouseTypeService } from '../services/house-type.service';
import { HouseType } from '../models/house-type';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-house-type',
  templateUrl: './list-house-type.component.html',
  styleUrls: ['./list-house-type.component.scss']
})
export class ListHouseTypeComponent implements OnInit {

  title: string;
  rows: HouseType[] = [];

  constructor(private houseTypeService: HouseTypeService,
    private router: Router) { }

  ngOnInit(): void {
    this.title = 'Development';
    this.getDevelopments();
  }

  getDevelopments() {
    this.houseTypeService.getHouseTypes().subscribe(
      result => {
        this.rows = result;
      }
    )
  }

  deleteHouseType(id: number) {
    if(confirm('Are you sure want to delete?')) {
      this.houseTypeService.deleteHouseType(id).subscribe(
        result => {
          console.log(result);
          if ( ! result.error) {
            this.rows = this.rows.filter(item => item.id != id)
          } else {
            alert('Some thingh went wrong!');
          }
        }
      )
    }
  }

}
