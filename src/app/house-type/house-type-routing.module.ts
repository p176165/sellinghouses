import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ListHouseTypeComponent } from './list-house-type/list-house-type.component';
import { AddHouseTypeComponent } from './add-house-type/add-house-type.component'; 

const routes: Routes = [ 
  {
  path: '',
  canActivate: [AuthGuard],
  canActivateChild: [AuthGuard],
  children: [
    {
      path: '',
      component: ListHouseTypeComponent
    },
    {
      path: 'create',
      component: AddHouseTypeComponent
    },
    {
      path: 'edit/:id',
      component: AddHouseTypeComponent 
    }
  ]
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseTypeRoutingModule { }
